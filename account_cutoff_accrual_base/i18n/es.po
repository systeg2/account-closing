# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * account_cutoff_accrual_base
#
# Translators:
# OCA Transbot <transbot@odoo-community.org>, 2017
# enjolras <yo@miguelrevilla.com>, 2017
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 10.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-05-25 02:35+0000\n"
"PO-Revision-Date: 2019-02-01 13:03+0000\n"
"Last-Translator: Marta Vázquez Rodríguez <vazrodmar@gmail.com>\n"
"Language-Team: Spanish (https://www.transifex.com/oca/teams/23907/es/)\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.4\n"

#. module: account_cutoff_accrual_base
#: model:ir.model,name:account_cutoff_accrual_base.model_account_cutoff
msgid "Account Cut-off"
msgstr "Operación de cierre de cuenta"

#. module: account_cutoff_accrual_base
#: model:ir.model,name:account_cutoff_accrual_base.model_account_cutoff_line
msgid "Account Cut-off Line"
msgstr "Línea de operación de cierre de cuenta"

#. module: account_cutoff_accrual_base
#: model:ir.model.fields,field_description:account_cutoff_accrual_base.field_account_config_settings_accrual_taxes
#: model:ir.model.fields,field_description:account_cutoff_accrual_base.field_res_company_accrual_taxes
msgid "Accrual On Taxes"
msgstr "Acumulación de impuestos"

#. module: account_cutoff_accrual_base
#: model:ir.actions.act_window,name:account_cutoff_accrual_base.account_expense_accrual_action
#: model:ir.ui.menu,name:account_cutoff_accrual_base.account_expense_accrual_menu
msgid "Accrued Expense"
msgstr "Gasto devengado"

#. module: account_cutoff_accrual_base
#: code:addons/account_cutoff_accrual_base/models/account_cutoff.py:51
#: model:ir.model.fields,field_description:account_cutoff_accrual_base.field_account_tax_account_accrued_expense_id
#, python-format
msgid "Accrued Expense Tax Account"
msgstr "Cuenta de impuestos de gasto devengado"

#. module: account_cutoff_accrual_base
#: model:ir.actions.act_window,name:account_cutoff_accrual_base.account_revenue_accrual_action
#: model:ir.ui.menu,name:account_cutoff_accrual_base.account_revenue_accrual_menu
msgid "Accrued Revenue"
msgstr "Ingreso devengado"

#. module: account_cutoff_accrual_base
#: code:addons/account_cutoff_accrual_base/models/account_cutoff.py:54
#: model:ir.model.fields,field_description:account_cutoff_accrual_base.field_account_tax_account_accrued_revenue_id
#, python-format
msgid "Accrued Revenue Tax Account"
msgstr "Cuenta de impuestos de ingreso devengado"

#. module: account_cutoff_accrual_base
#: model:ir.actions.act_window,help:account_cutoff_accrual_base.account_expense_accrual_action
msgid "Click to start preparing a new expense accrual."
msgstr "Pulse para preparar un nuevo gasto devengado."

#. module: account_cutoff_accrual_base
#: model:ir.actions.act_window,help:account_cutoff_accrual_base.account_revenue_accrual_action
msgid "Click to start preparing a new revenue accrual."
msgstr "Pulse para preparar un nuevo ingreso devengado."

#. module: account_cutoff_accrual_base
#: model:ir.model,name:account_cutoff_accrual_base.model_res_company
msgid "Companies"
msgstr "Empresas"

#. module: account_cutoff_accrual_base
#: model:ir.model.fields,field_description:account_cutoff_accrual_base.field_account_config_settings_default_accrued_expense_account_id
#: model:ir.model.fields,field_description:account_cutoff_accrual_base.field_res_company_default_accrued_expense_account_id
msgid "Default Account for Accrued Expenses"
msgstr "Cuenta predeterminada para gastos devengados"

#. module: account_cutoff_accrual_base
#: model:ir.model.fields,field_description:account_cutoff_accrual_base.field_account_config_settings_default_accrued_expense_return_account_id
#: model:ir.model.fields,field_description:account_cutoff_accrual_base.field_res_company_default_accrued_expense_return_account_id
msgid "Default Account for Accrued Expenses Returns"
msgstr "Cuenta predeterminada para devoluciones de gastos acumulados"

#. module: account_cutoff_accrual_base
#: model:ir.model.fields,field_description:account_cutoff_accrual_base.field_account_config_settings_default_accrued_revenue_account_id
#: model:ir.model.fields,field_description:account_cutoff_accrual_base.field_res_company_default_accrued_revenue_account_id
msgid "Default Account for Accrued Revenues"
msgstr "Cuenta predeterminada para ingresos devengados"

#. module: account_cutoff_accrual_base
#: model:ir.model.fields,field_description:account_cutoff_accrual_base.field_account_config_settings_default_accrued_revenue_return_account_id
#: model:ir.model.fields,field_description:account_cutoff_accrual_base.field_res_company_default_accrued_revenue_return_account_id
msgid "Default Account for Accrued Revenues Returns"
msgstr "Cuenta predeterminada para ingresos devengados"

#. module: account_cutoff_accrual_base
#: model:ir.model.fields,field_description:account_cutoff_accrual_base.field_account_config_settings_default_accrual_expense_journal_id
#: model:ir.model.fields,field_description:account_cutoff_accrual_base.field_res_company_default_accrual_expense_journal_id
msgid "Default Journal for Accrued Expenses"
msgstr "Diario predeterminado para gastos devengados"

#. module: account_cutoff_accrual_base
#: model:ir.model.fields,field_description:account_cutoff_accrual_base.field_account_config_settings_default_accrual_revenue_journal_id
#: model:ir.model.fields,field_description:account_cutoff_accrual_base.field_res_company_default_accrual_revenue_journal_id
msgid "Default Journal for Accrued Revenues"
msgstr "Diario predeterminado para ingresos devengados"

#. module: account_cutoff_accrual_base
#: code:addons/account_cutoff_accrual_base/models/account_cutoff.py:56
#, python-format
msgid "Missing '%s' on tax '%s'."
msgstr ""

#. module: account_cutoff_accrual_base
#: model:ir.model.fields,help:account_cutoff_accrual_base.field_account_cutoff_line_price_unit
msgid "Price per unit (discount included)"
msgstr "Precio unitario (descuento incluido)"

#. module: account_cutoff_accrual_base
#: model:ir.model.fields,field_description:account_cutoff_accrual_base.field_account_cutoff_line_quantity
msgid "Quantity"
msgstr "Cantidad"

#. module: account_cutoff_accrual_base
#: model:ir.model,name:account_cutoff_accrual_base.model_account_tax
msgid "Tax"
msgstr "Impuestos"

#. module: account_cutoff_accrual_base
#: model:ir.actions.act_window,help:account_cutoff_accrual_base.account_expense_accrual_action
msgid ""
"This view can be used by accountants in order to collect information about "
"accrued expenses. It then allows to generate the corresponding cut-off "
"journal entry in one click."
msgstr ""
"Los contables pueden utilizar esta vista para obtener información sobre los "
"gastos devengados. Después les permitirá generar la entrada de operación de "
"cierre correspondiente en una única acción."

#. module: account_cutoff_accrual_base
#: model:ir.actions.act_window,help:account_cutoff_accrual_base.account_revenue_accrual_action
msgid ""
"This view can be used by accountants in order to collect information about "
"accrued revenue. It then allows to generate the corresponding cut-off "
"journal entry in one click."
msgstr ""
"Los contables pueden utilizar esta vista para obtener información sobre los "
"ingresos devengados. Después les permitirá generar la entrada de operación "
"de cierre correspondiente en una única acción."

#. module: account_cutoff_accrual_base
#: model:ir.model.fields,field_description:account_cutoff_accrual_base.field_account_cutoff_line_price_unit
msgid "Unit Price"
msgstr "Precio unitario"

#. module: account_cutoff_accrual_base
#: model:ir.model,name:account_cutoff_accrual_base.model_account_config_settings
msgid "account.config.settings"
msgstr "account.config.settings"
